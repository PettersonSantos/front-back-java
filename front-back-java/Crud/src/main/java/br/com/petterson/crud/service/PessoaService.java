package br.com.petterson.crud.service;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.petterson.crud.model.Pessoa;
import br.com.petterson.crud.repository.PessoaRepository;

@Service
public class PessoaService {
	
	@Autowired
	PessoaRepository pessoaRepository;
	
	
	public Pessoa salvar(Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}
	
	public Collection<Pessoa> buscarTodos(){
		return pessoaRepository.findAll();
		
	}
	
	public void excluir(Pessoa pessoaEncontrada) {
		pessoaRepository.delete(pessoaEncontrada);
	}
	public Pessoa buscarPorId(Integer id) {
		return pessoaRepository.getOne(id);
	}
	
}
