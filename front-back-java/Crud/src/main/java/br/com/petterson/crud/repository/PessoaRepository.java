package br.com.petterson.crud.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



import br.com.petterson.crud.model.Pessoa;



@Repository
public interface PessoaRepository extends JpaRepository <Pessoa, Integer>{
	

}
