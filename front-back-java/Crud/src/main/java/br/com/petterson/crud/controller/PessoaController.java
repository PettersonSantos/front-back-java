package br.com.petterson.crud.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.petterson.crud.model.Pessoa;
import br.com.petterson.crud.service.PessoaService;

@RestController
public class PessoaController {
	
	@Autowired
	PessoaService pessoaService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/pessoa", consumes = MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pessoa> cadastrarPessoa(@RequestBody Pessoa pessoa){
		Pessoa pessoaCadastrada = pessoaService.salvar(pessoa);
		return new ResponseEntity<>(pessoaCadastrada, HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/pessoa", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Pessoa>> buscaTodosClientes(){
		Collection<Pessoa> pessoasBuscadas = pessoaService.buscarTodos();
		return new ResponseEntity<>(pessoasBuscadas, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/pessoa/{id}")
	public ResponseEntity<Pessoa> excluirPessoa(@PathVariable Integer id){
		
		Pessoa pessoaEncontrada = pessoaService.buscarPorId(id);
		if(pessoaEncontrada == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		pessoaService.excluir(pessoaEncontrada);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/pessoa", consumes = MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pessoa> alterarPessoa(@RequestBody Pessoa pessoa){
		
		Pessoa pessoaAlterada = pessoaService.salvar(pessoa);
		
		return new ResponseEntity<>(pessoaAlterada, HttpStatus.OK);
		
	}
	

}
