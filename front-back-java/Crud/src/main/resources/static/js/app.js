var appCrud = angular.module("appCrud", []);

appCrud.controller("pessoaController", function($scope, $http){
	
	$scope.pessoas=[];
	$scope.pessoa={};
	
	$scope.carregarPessoa = function(){
		$http({method: 'GET', url: '/pessoa'})
		.then(function (response){
			$scope.pessoas = response.data;
		},
		function(response){
			console.log(response.data);
			console.log(response.status);
		});
	};
	
	$scope.salvarPessoa = function(){
		$http({method: 'POST', url: '/pessoa', data:$scope.pessoa})
		.then(function (response){
			$scope.carregarPessoa();
			$scope.pessoa={};
		},
		function(response){
			console.log(response.data);
			console.log(response.status);
		});
	};
	
	$scope.excluirPessoa = function(pessoa){
		$http({method: 'DELETE', url: '/pessoa/'+ pessoa.id})
		.then(function (response){
			
			pos= $scope.pessoas.indexOf(pessoa);
			$scope.pessoas.splice(pos, 1);
		},
		function(response){
			console.log(response.data);
			console.log(response.status);
		});
	};
	
	$scope.editarPessoa = function(pes){
		$scope.pessoa = angular.copy(pes);
	};
	
	$scope.cancelarEdicao = function(){
		$scope.pessoa={};
	};
	
	$scope.carregarPessoa();
		
});

